import React, { useState } from "react";

function ReadMore({ text, maxLength, lang }) {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggleReadMore = () => {
    setIsExpanded(!isExpanded);
  };

  const truncatedText = isExpanded ? text : text.slice(0, maxLength);

  return (
    <div>
      <div>{truncatedText}</div>
      {text.length > maxLength && lang === "EN" ? (
        <p className="text-primary cursor-pointer" onClick={toggleReadMore}>
          {isExpanded ? "Read Less" : "...Read More"}
        </p>
      ) : text.length > maxLength && lang === "TH" ? (
        // <button onClick={toggleReadMore} className="read-more-button">
        //   {isExpanded ? 'Read Less' : 'Read More'}
        // </button>
        <p className="text-primary cursor-pointer" onClick={toggleReadMore}>
          {isExpanded ? "ปิด" : "...ดูเพิ่มเติม"}
        </p>
        // <button onClick={toggleReadMore} className="read-more-button">
        //   {isExpanded ? 'Read Less' : 'Read More'}
        // </button>
      ) : ""}
    </div>
  );
}

export default ReadMore;
