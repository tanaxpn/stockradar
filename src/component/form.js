import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import isEmail from "validator/lib/isEmail";
import Swal from "sweetalert2";

function Form() {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);

  const [formData, setFormData] = useState({
    name: searchParams.get("name") || "",
    surname: searchParams.get("surname") || "",
    phone: searchParams.get("phone") || "",
    email: searchParams.get("email") || "",
    ref: searchParams.get("ref") || "",
  });

  const [errors, setErrors] = useState({
    email: "",
    phone: "",
  });

  useEffect(() => {}, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    if (name === "phone") {
      const numericValue = value.replace(/\D/g, "");
      setFormData({ ...formData, [name]: numericValue });
    } else {
      setFormData({ ...formData, [name]: value });
    }
    if (name === "email") {
      setErrors({
        ...errors,
        email: isEmail(value) ? "" : "Invalid email format",
      });
    }
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    if (!formData.name) {
      Swal.fire({
        title: "กรุณาระบุชื่อ",
        icon: "warning",
        confirmButtonText: "OK",
      });
      return;
    } else if (!formData.surname) {
      Swal.fire({
        title: "กรุณาระบุนามสกุล",
        icon: "warning",
        confirmButtonText: "OK",
      });
      return;
    } else if (!formData.phone || errors.phone || formData?.phone.length < 10) {
      Swal.fire({
        title: "กรุณาระบุเบอร์หรือกรอกให้ครบ 10 หลัก",
        icon: "warning",
        confirmButtonText: "OK",
      });
      return;
    }
    if (!formData.email || errors.email) {
      Swal.fire({
        title: "กรุณาระบุอีเมลหรือกรอกให้ถูกต้อง",
        icon: "warning",
        confirmButtonText: "OK",
      });
      return;
    } else if (!formData.ref) {
      Swal.fire({
        title: "กรุณาระบุ Ref",
        icon: "warning",
        confirmButtonText: "OK",
      });
      return;
    } else {
      Swal.fire({
        title: "สำเร็จ",
        icon: "success",
        confirmButtonText: "OK",
      });
      return;
    }
  };

  return (
    <>
      <form className="mb-5" onSubmit={handleSubmit}>
        <div className="row g-0">
          <div className="col-3 col-md-4 col-lg-6 text-end pe-2">
            <label>Name:</label>
          </div>
          <div className="col-9 col-md-8 col-lg-6 text-start">
            <input
              type="text"
              name="name"
              value={formData.name}
              style={formData?.name.length === 0 ? { borderColor: "red" } : {}}
              onChange={handleInputChange}
            />
          </div>
          <div className="my-2"></div>
          <div className="col-3 col-md-4 col-lg-6 text-end pe-2">
            <label>Surname:</label>
          </div>
          <div className="col-9 col-md-8 col-lg-6 text-start">
            <input
              type="text"
              name="surname"
              value={formData.surname}
              style={
                formData?.surname.length === 0 ? { borderColor: "red" } : {}
              }
              onChange={handleInputChange}
            />
          </div>
          <div className="my-2"></div>
          <div className="col-3 col-md-4 col-lg-6 text-end pe-2">
            <label>Phone:</label>
          </div>
          <div className="col-9 col-md-8 col-lg-6 text-start">
            <input
              type="tel"
              name="phone"
              value={formData.phone}
              style={formData?.phone.length === 0 ? { borderColor: "red" } : {}}
              maxLength={10}
              onChange={handleInputChange}
            />
          </div>
          <div className="my-2"></div>
          <div className="col-3 col-md-4 col-lg-6 text-end pe-2">
            <label>Email:</label>
          </div>
          <div className="col-9 col-md-8 col-lg-6 text-start">
            <input
              type="text"
              name="email"
              value={formData.email}
              style={formData?.email.length === 0 ? { borderColor: "red" } : {}}
              onChange={handleInputChange}
            />
          </div>
          <div className="my-2"></div>
          <div className="col-3 col-md-4 col-lg-6 text-end pe-2">
            <label>Ref:</label>
          </div>
          <div className="col-9 col-md-8 col-lg-6 text-start">
            <input
              type="text"
              name="ref"
              value={formData.ref}
              style={formData?.ref.length === 0 ? { borderColor: "red" } : {}}
              onChange={handleInputChange}
            />
          </div>
        </div>
        <button className="btn btn-primary mt-2" type="submit">
          submit
        </button>
      </form>
    </>
  );
}
export default Form;
