import React, { useState } from "react";
import ReadMore from "./readmore";

import thLogo from "../img/th.png";
import enLogo from "../img/en.png";

function Stock({ data }) {
  const [lang, setLang] = useState("TH");

  const maxLength = 50;

  const numberFormat = (value) => {
    let number = parseFloat(value).toFixed(2);
    const options = {
      minimumFractionDigits: 2,
      maximumFractionDigits: 2,
    };
    var withCommas = Number(number).toLocaleString("en", options);
    return withCommas;
  };

  const changeLang = () => {
    if (lang === "TH") {
      setLang("EN");
    } else {
      setLang("TH");
    }
  };

  return (
    <div className="container">
      <div className="table-responsive-xs table-responsive-sm position-relative overflow-x-auto">
        <button
          onClick={changeLang}
          type="button"
          className="position-fixed btn btn-primary"
          style={{ top: "80px", right: "10px", zIndex: 5 }}
        >
          <img
            src={lang === "TH" ?  thLogo  : enLogo }
            alt="Logo"
            width="30"
            height="24"
            className="d-inline-block align-text-top"
          />
        </button>
        {data ? (
          <table
            className="table table-striped table-bordered table-hover"
            style={{ width: "100%" }}
          >
            <thead>
              <tr>
                <th>No.</th>
                <th>Name</th>
                <th style={{ minWidth: "125px" }}>Short Name</th>
                <th style={{ minWidth: "250px" }}>Company name</th>
                <th style={{ minWidth: "500px" }}>Business Type</th>
                <th>Type</th>
                <th>Cap</th>
                <th>URL</th>
              </tr>
            </thead>
            <tbody>
              {data.map((item, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.N_name}</td>
                  <td>{item.N_shortname || "-"}</td>
                  <td className="text-start">
                    <ReadMore
                      key={index}
                      text={lang === "TH" ? item.N_COMPANY_T : item.N_COMPANY_E}
                      maxLength={maxLength}
                      lang={lang}
                    />
                  </td>
                  <td className="text-start">
                    <ReadMore
                      key={index}
                      text={
                        lang === "TH"
                          ? item.N_BUSINESS_TYPE_T
                          : item.N_BUSINESS_TYPE_E
                      }
                      maxLength={maxLength}
                      lang={lang}
                    />
                  </td>
                  <td>{item.F_TYPE}</td>

                  <td>{numberFormat(item.marketcap)}</td>
                  <td>
                    <a
                      href={item.N_URL}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {item.N_URL}
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <div>No data available</div>
        )}
      </div>
    </div>
  );
}

export default Stock;
