import React, { useEffect, useState } from "react";
import logo from "./logo.svg";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import axios from "axios";

import "./App.css";
import Stock from "./component/stock";
import Form from "./component/form";

function App() {
  const [data, setData] = useState(null);

  async function fetchData() {
    try {
      const response = await axios.get(
        "https://stockradars.co/assignment/data.php"
      );
      setData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="App">
      <header className="header-fix-bar">
        <nav className="navbar navbar-expand-lg ">
          <div className="container-fluid">
            <a className="navbar-brand text-light" href="/">
              <img
                src={logo}
                alt="Logo"
                width="30"
                height="24"
                className="d-inline-block align-text-top"
              />
              <span className="ms-2">StockRadars</span>
            </a>
            <button
              className="navbar-toggler bg-white"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon text-white"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <a
                    className="nav-link active text-white"
                    aria-current="page"
                    href="/"
                  >
                    List
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link text-white"
                    href="/form?name=Tanakrit&surname=Nammakuna&phone=0952362199&email=tanaxpn@gmail.com&ref=stockradars"
                  >
                    Form
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        {/* <nav className="navbar">
          <div className="container-fluid">
            
          </div>
        </nav> */}
      </header>
      <div className="paddingMain">
        <Router>
          <Routes>
            <Route path="/" element={<Stock data={data} />} />
            <Route path="/form" element={<Form />} />
          </Routes>
        </Router>
      </div>
    </div>
  );
}

export default App;
